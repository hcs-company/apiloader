FROM quay.io/fedora/fedora:37 AS builder
USER root
WORKDIR /build
COPY src /build
RUN dnf -y install @c-development
RUN dnf -y install upx python3 python3-pip
RUN pip3 install -r requirements.txt
RUN pyinstaller -s -F app.py
RUN mkdir -p /target/hcs-company.com
RUN mkdir -m 1777 /target/tmp
RUN staticx /build/dist/app /target/hcs-company.com/app; chmod 755 /target/hcs-company.com/app


FROM scratch
WORKDIR /
USER root
COPY --from=builder /target /
CMD ["/hcs-company.com/app"]
USER 1001
