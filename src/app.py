#!/usr/bin/env python3
from kubernetes import client, config, watch
import sys
import os
import time
import threading

INTERVAL=int(os.getenv('INTERVAL', 10))
THREADS=int(os.getenv('THREADS', 10))

sys.stdout.write(f'Starting up with {THREADS} threads and {INTERVAL}ms interval\n')

breakybreak = threading.Event()

if os.getenv('KUBERNETES_SERVICE_HOST'):
    sys.stdout.write('Running in cluster\n')
    config.load_incluster_config()
    namespace = open("/var/run/secrets/kubernetes.io/serviceaccount/namespace").read()
else:
    sys.stdout.write('Running standalone\n')
    config.load_kube_config()
    namespace = config.list_kube_config_contexts()[1]['context']['namespace']
v1 = client.CoreV1Api()

sys.stdout.write(f'Using namespace {namespace}\n')
sys.stdout.flush()

def pod_list(interval, tasknum):
  sys.stdout.write(f'Starting infinite pod_list thread {tasknum}\n')
  while True:
    pod_list = v1.list_namespaced_pod(namespace)
    if breakybreak.is_set():
      sys.stdout.write(f'Terminating thread {tasknum}\n')
      break
    time.sleep(INTERVAL * 0.0001)

if __name__ == '__main__':
  threads = []
  for i in range(THREADS):
    threads.append(threading.Thread(target=pod_list, args=(INTERVAL, i)))
  for thread in threads:
    thread.start()
  try:
    dummy_event = threading.Event()
    dummy_event.wait()
  except:
    sys.stdout.write('Exiting, please hold...\n')
    pass
  finally:
    sys.stdout.write(f'Trying to escape\n')
    breakybreak.set()
    for thread in threads:
      thread.join()
    sys.stdout.slush()
